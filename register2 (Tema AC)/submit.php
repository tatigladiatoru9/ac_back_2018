<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$study = "";
$sex_m = "M";
$sex_f = "F";
$error = 0;
$error_text = "";
$link=mysqli_connect("127.0.0.1" , "root" , "" , "ac");
$limit_nr_user = 49;
$q_nr_user = "SELECT COUNT(*) as nr_total FROM register2";



if(isset($_POST['study'])){
	$study = $_POST['study'];
}

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check) || empty($study) ) {

	$error = 1;
	$error_text = "You haven't completed all of the required fields";
}

if(strlen($firstname) < 3 || strlen($lastname) < 3) {
	$error = 1;
	$error_text = "Name too short!";
}

if(is_numeric($firstname) || is_numeric($lastname)) {
	$error = 1;
	$error_text = "Name can't contain numbers!";
}

if(strlen($question) < 15) {
	$error = 1;
	$error_text ="Answer to the question is too short!";
}

if(strlen($phone) != 10 || !is_numeric($phone)) {
	$error = 1;
	$error_text = "Invalid phone number";
}

if (filter_var($email , FILTER_VALIDATE_EMAIL) == false) {
	$error = 1;
	$error_text = "Not a valid email form!";
}
//RESTRICTIE ACELASI EMAIL
$q_email = "SELECT email FROM register2 WHERE email = '$email'";
$rezultat = mysqli_query($link , $q_email);
$rezultat_email = mysqli_num_rows($rezultat);

if ($rezultat_email >= 1) 
 {
	$error = 1;
	$error_text = "Email already used!";
}
//

if (filter_var($facebook , FILTER_VALIDATE_URL) == false) {
	$error = 1;
	$error_text = "Not a valid facebook page!";
}

if($captcha_inserted != $captcha_generated){
	$error = 1;
	$error_text = "Captcha inserted is not correct!";
}

if(strlen($cnp) != 13) {
	$error = 1;
	$error_text = "CNP must have 13 characters";
}


if(intval($cnp[0]) > 6 || $cnp[0] < 1){
	$error = 1;
	$error_text = "The first element of the CNP must be within 1-6";
}

if (strlen($study) <= 3 || strlen($study) > 30 ){
	$error = 1;
	$error_text = "Your faculty must have up to 3 characters and max 30";
}
if (is_numeric($study)){
	$error = 1;
	$error_text = "Your faculty cannot contain numbers!";
}

if ($birth >= 2000 || $birth < 1918) {
	$error = 1;
	$error_text = "You must have in between 18-100 years old";
}
//VALIDARE CNP IN FUNCTIE DE DATA DE NASTERE
$birth_s = str_split($birth);
$cnp_s = str_split($cnp);

$calcnp  = ($cnp_s[1] * 10) + $cnp_s[2];
$calnastere = ($birth_s[2] * 10) + $birth_s[3];

if ($calcnp != $calnastere) {
	$error = 1;
	$error_text = "Your birthdate and cnp are not compatible";
}
//
// ARRAY PT SEX
$masculin = array(1 , 3 , 5);
$feminin = array(2 , 4 , 6);
//
try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

$max_users_q = mysqli_query($link , $q_nr_user);
$max_users_q_rez = mysqli_fetch_assoc($max_users_q);
if ($max_users_q_rez["nr_total"] >= $limit_nr_user) {
	$error = 1;
	$error_text = "The maximum number of users has been reached";
}


if ($error == 0){
//INTRODUCERE IN BAZA DE DATE IN FUNCTIE DE SEX (NU STIU CAT DE OPTIM ESTE, DAR MERGE! xD)
foreach ($masculin as $value => $m_values){
if (intval($cnp[0]) == $m_values) {
	$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,study,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:study, :sex_m)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':study' , $study);
$stmt2 -> bindParam(':sex_m' , $sex_m);


}

}

foreach($feminin as $value2 => $f_values) {
	if (intval($cnp[0]) == $f_values) {
		$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,study,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:study, :sex_f)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':study' , $study);
$stmt2 -> bindParam(':sex_f' , $sex_f);
	}
}
//
if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

  }
  else

    echo "Succes";
 
}
 else {
 	echo $error_text;
 }
?>


