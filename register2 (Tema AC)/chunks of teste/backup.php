<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$study = "";
$sex_m = "M";
$sex_f = "F";
$error = 0;
$error_text = "";


if(isset($_POST['study'])){
	$study = $_POST['study'];
}

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check) || empty($study) ) {

	$error = 1;
	$error_text = "You haven't completed all of the required fields";
}

if(strlen($firstname) < 3 || strlen($lastname) < 3) {
	$error = 1;
	$error_text = "Name too short!";
}

if(is_numeric($firstname) || is_numeric($lastname)) {
	$error = 1;
	$error_text = "Name can't contain numbers!";
}

if(strlen($question) < 15) {
	$error = 1;
	$error_text ="Answer to the question is too short!";
}

if(strlen($phone) != 10 || !is_numeric($phone)) {
	$error = 1;
	$error_text = "Invalid phone number";
}

if (filter_var($email , FILTER_VALIDATE_EMAIL) == false) {
	$error = 1;
	$error_text = "Not a valid email form!";
}



if (filter_var($facebook , FILTER_VALIDATE_URL) == false) {
	$error = 1;
	$error_text = "Not a valid facebook page!";
}

if($captcha_inserted != $captcha_generated){
	$error = 1;
	$error_text = "Captcha inserted is not correct!";
}

if(strlen($cnp) != 13) {
	$error = 1;
	$error_text = "CNP must have 13 characters";
}


if(intval($cnp[0]) > 6 || $cnp[0] < 1){
	$error = 1;
	$error_text = "The first element of the CNP must be within 1-6";
}

if (strlen($study) <= 3 || strlen($study) > 30 ){
	$error = 1;
	$error_text = "Your faculty must have up to 3 characters and max 30";
}
if (is_numeric($study)){
	$error = 1;
	$error_text = "Your faculty cannot contain numbers!";
}

if ($birth >= 2000 || $birth < 1918) {
	$error = 1;
	$error_text = "You must have in between 18-100 years old";
}
$masculin = array ();
for ($masculin = -1; $masculin <= 5; $masculin+2;)

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}


if ($error == 0){


if (intval($cnp[0]) == 1){
	$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,study,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:study,:sex_m)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':study' , $study);
$stmt2 -> bindParam(':sex_m' , $sex_m);
}

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

  }
  else

    echo "Succes";
 
}
 else {
 	echo $error_text;
 }
?>


